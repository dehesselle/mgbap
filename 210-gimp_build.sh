#!/usr/bin/env bash
#
# SPDX-FileCopyrightText: 2021 René de Hesselle <dehesselle@web.de>
#
# SPDX-License-Identifier: GPL-2.0-or-later

### description ################################################################

# Build and install GIMP.

### shellcheck #################################################################

# Nothing here.

### dependencies ###############################################################

source "$(dirname "${BASH_SOURCE[0]}")"/jhb/etc/jhb.conf.sh

bash_d_include error

### variables ##################################################################

SELF_DIR=$(dirname "${BASH_SOURCE[0]}")

### functions ##################################################################

# Nothing here.

### main #######################################################################

error_trace_enable

#----------------------------------------------------------- (re-) configure jhb

# Rerun configuration to adapt to the current system. This will
#   - allow GIMP to be build against a different SDK than the toolset has
#     been built with
#   - setup ccache

jhb configure "$SELF_DIR"/modulesets/gtk-osx.modules

#-------------------------------------------------------------------- build GIMP

jhb build gimp
